package com.reksa.karang.demosqlite1;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.reksa.karang.demosqlite1.database.DatabaseHelper;

public class DetailDataActivity extends AppCompatActivity {
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    TextView textNomor, textNama, textTanggal, textJenisKelamin, textAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DatabaseHelper(this);
        textNomor = findViewById(R.id.textNomor);
        textNama = findViewById(R.id.textNama);
        textTanggal = findViewById(R.id.textDate);
        textJenisKelamin = findViewById(R.id.textJenisKelamin);
        textAlamat = findViewById(R.id.textAlamat);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM biodata WHERE nama='" +
                getIntent().getStringExtra("nama") + "'", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            textNomor.setText(cursor.getString(0));
            textNama.setText(cursor.getString(1));
            textTanggal.setText(cursor.getString(2));
            textJenisKelamin.setText(cursor.getString(3));
            textAlamat.setText(cursor.getString(4));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
