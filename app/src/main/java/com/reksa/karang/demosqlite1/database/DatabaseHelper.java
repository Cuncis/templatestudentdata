package com.reksa.karang.demosqlite1.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    private static final String DATABASE_NAME = "student.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE biodata(id INTEGER PRIMARY KEY, nama TEXT NULL, tanggal TEXT NULL, " +
                "jk TEXT NULL, alamat TEXT NULL);";
        db.execSQL(sql);
        Log.d(TAG, "onCreate: " + sql);
        sql = "INSERT INTO biodata (no, nama, tanggal, jk, alamat) VALUES " +
                "('0', 'Jalaludin', '1996-07-11', 'Lakilaki','Rangkasbitung');";
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS biodata");
        onCreate(db);
    }
}
