package com.reksa.karang.demosqlite1;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.reksa.karang.demosqlite1.database.DatabaseHelper;

public class InputDataActivity extends AppCompatActivity {

    protected Cursor cursor;
    DatabaseHelper dbHelper;
    Button btn;
    EditText editNomor, editNama, editTanggal, editJk, editAlamat;
    String edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DatabaseHelper(this);
        editNomor = findViewById(R.id.editNomor);
        editNama = findViewById(R.id.editNama);
        editTanggal = findViewById(R.id.editDate);
        editJk = findViewById(R.id.editJenisKelamin);
        editAlamat = findViewById(R.id.editAlamat);
        btn = findViewById(R.id.btnSimpan);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                edit = editNomor.getText().toString();
                edit = editNama.getText().toString();
                edit = editTanggal.getText().toString();
                edit = editJk.getText().toString();
                edit = editAlamat.getText().toString();

                if (edit.isEmpty()) {
                    Toast.makeText(InputDataActivity.this, "Kolom tidak boleh kosong...", Toast.LENGTH_SHORT).show();
                } else {
                    db.execSQL("insert into biodata(id, nama, tanggal, jk, alamat) values('" +
                            editNomor.getText().toString() + "','" +
                            editNama.getText().toString() + "','" +
                            editTanggal.getText().toString() + "','" +
                            editJk.getText().toString() + "','" +
                            editAlamat.getText().toString() + "')");
                    Toast.makeText(getApplicationContext(), "Data Tersimpan...", Toast.LENGTH_LONG).show();
                    finish();
                }

                DataMahasiswaActivity.da.refreshList();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
