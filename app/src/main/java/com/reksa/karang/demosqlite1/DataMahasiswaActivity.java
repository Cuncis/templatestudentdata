package com.reksa.karang.demosqlite1;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.reksa.karang.demosqlite1.database.DatabaseHelper;

public class DataMahasiswaActivity extends AppCompatActivity {

    String[] daftar;
    ListView listView;
    protected Cursor cursor;
    DatabaseHelper dbHelper;
    public static DataMahasiswaActivity da;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_mahasiswa);

        Button btn = findViewById(R.id.btn_input_data);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DataMahasiswaActivity.this, InputDataActivity.class);
                startActivity(intent);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        da = this;
        dbHelper = new DatabaseHelper(this);
        refreshList();
    }

    public void refreshList() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM biodata", null);
        daftar = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int  i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            daftar[i] = cursor.getString(1);
        }
        listView = findViewById(R.id.listView);
        listView.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, daftar));
        listView.setSelected(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String selection = daftar[position];
                final CharSequence[] dialogItems = {"Lihat Data", "Update Data", "Hapus Data"};
                new AlertDialog.Builder(DataMahasiswaActivity.this)
                        .setTitle("Pilihan")
                        .setItems(dialogItems, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        Intent i = new Intent(getApplicationContext(), DetailDataActivity.class);
                                        i.putExtra("nama", selection);
                                        startActivity(i);
                                        break;
                                    case 1:
                                        i = new Intent(getApplicationContext(), UpdateDataActivity.class);
                                        i.putExtra("nama", selection);
                                        startActivity(i);;
                                        break;
                                    case 2:
                                        SQLiteDatabase db = dbHelper.getWritableDatabase();
                                        db.execSQL("DELETE FROM biodata WHERE nama='" + selection + "'");
                                        refreshList();
                                        break;
                                }
                            }
                        }).create().show();
            }
        });
        ((ArrayAdapter)listView.getAdapter()).notifyDataSetInvalidated();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}






















